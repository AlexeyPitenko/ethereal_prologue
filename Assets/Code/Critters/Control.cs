﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class Control : MonoBehaviour
{
    public ControllableCritter controllableCritter;

    public CritterMovementComponent critterMovementController;

    void Start()
    {
        critterMovementController = GetComponent<CritterMovementComponent>();
    }
}

public class ControlSystem : ComponentSystem
{
    public struct State
    {
        public Control control;
    }

    protected override void OnUpdate()
    {
        var entities = GetEntities<State>();

        Vector3 desire = new Vector3(Input.GetAxisRaw("Horizontal"), 0, Input.GetAxisRaw("Vertical"));
        bool isJumpButtonPressed = Input.GetButtonDown("Jump");

        foreach (var entity in entities)
        {
            ControllableCritter controllableCritter = entity.control.controllableCritter;

            if (controllableCritter)
            {
                controllableCritter.SetDesire(desire);

                if (isJumpButtonPressed && entity.control.critterMovementController.isOnFloor)
                {
                    controllableCritter.Jump();
                }
            }
        }
    }
}
