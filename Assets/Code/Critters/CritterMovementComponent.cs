﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class CritterMovementComponent : MonoBehaviour
{
    public Vector3 legs = new Vector3(0, -0.5f, 0);

    // public float legsHeight = 0.1f;

    // How fat character is
    public float radius = 0.5f;

    // Where you can go without jumping
    public float slopeHeight = 0.20f;

    // Where you should jump
    public float jumpHeight = 0.35f;

    // Is standing on a solid ground and can jump
    public bool isOnFloor = false;

    // Should jump
    public bool shouldJump = false;

    public const float verticalBumper = 0.01f;
    public const float horizontalBumper = 0.01f;


    public Vector3 storedImpulse = new Vector3(0, 0, 0);

    public ControllableCritter controllableCritter;

    void Start()
    {
        controllableCritter = GetComponent<ControllableCritter>();
    }

    public void Move(Vector3 impulse)
    {
        storedImpulse += impulse;
    }
}

[UpdateAfter(typeof(ControlSystem))]
[UpdateAfter(typeof(ControllableCritter))]
public class CritterMovementComponentSystem : ComponentSystem
{
    public struct State
    {
        public Transform transform;
        public CritterMovementComponent movement;
        public ControllableCritter controllableCritter;
    }

    protected override void OnUpdate()
    {
        var entities = GetEntities<State>();

        foreach (var entity in entities)
        {
            Transform transform = entity.transform;
            CritterMovementComponent movement = entity.movement;
            ControllableCritter controllableCritter = entity.controllableCritter;

            // Movement cast
            {
                Vector3 translate = Vector3.zero;
                Vector3 origin = transform.position + movement.legs;
                Vector3 horizontalVector = new Vector3(movement.storedImpulse.x, 0, movement.storedImpulse.z);
                // Vector3 horizontalVector = new Vector3(.01f, 0, .01f);

                if (horizontalVector.magnitude > 0.0f)
                {
                    // Cast to next point
                    RaycastHit toNextPointHit;
                    bool toNextPoint = Physics.Raycast(origin, horizontalVector.normalized, out toNextPointHit, horizontalVector.magnitude + CritterMovementComponent.horizontalBumper);
                    Debug.DrawRay(origin, horizontalVector, Color.green, 1, true);

                    if (toNextPoint)
                    {
                        if (movement.isOnFloor)
                        {
                            // get up for slopeHeight
                            // cast to next point
                            bool toNextPointSlope = Physics.Raycast(origin + new Vector3(0, movement.slopeHeight, 0), horizontalVector.normalized, horizontalVector.magnitude + CritterMovementComponent.horizontalBumper);
                            Debug.DrawRay(origin + new Vector3(0, movement.slopeHeight, 0), horizontalVector, Color.yellow, 1, true);

                            if (toNextPointSlope)
                            {
                                bool toNextPointJump = Physics.Raycast(origin + new Vector3(0, movement.jumpHeight, 0), horizontalVector.normalized, horizontalVector.magnitude + CritterMovementComponent.horizontalBumper);
                                Debug.DrawRay(origin + new Vector3(0, movement.jumpHeight, 0), horizontalVector, Color.red, 1, true);

                                if (toNextPointJump)
                                {
                                    // Nothing we can do here. Place character at the end of raycast.
                                    // Stop motion
                                    translate += horizontalVector.normalized * (toNextPointHit.distance - CritterMovementComponent.horizontalBumper);
                                }
                                else
                                {
                                    movement.shouldJump = true;
                                    // isOnFloor = false;
                                    // Apply motion
                                }
                            }
                            else
                            {
                                // Cast down from next point
                                RaycastHit castDownFromNextPointSlopeHit;
                                bool castDownFromNextPointSlope = Physics.Raycast(origin + new Vector3(0, movement.slopeHeight, 0) + horizontalVector, Vector3.down, out castDownFromNextPointSlopeHit, movement.slopeHeight + CritterMovementComponent.verticalBumper);
                                Debug.DrawRay(origin + new Vector3(0, movement.slopeHeight, 0) + horizontalVector, Vector3.down * movement.slopeHeight, Color.blue, 1, true);

                                if (castDownFromNextPointSlope)
                                {
                                    // place character here
                                    translate += new Vector3(0, movement.slopeHeight - castDownFromNextPointSlopeHit.distance + CritterMovementComponent.verticalBumper, 0);
                                }
                                else
                                {
                                    movement.shouldJump = movement.isOnFloor;
                                    // isOnFloor = false;
                                }
                                // Apply motion
                                translate += horizontalVector;
                            }
                        }
                        else
                        {
                            // Nothing we can do here. Place character at the end of raycast.
                            // Stop motion
                            translate += horizontalVector.normalized * (toNextPointHit.distance - CritterMovementComponent.horizontalBumper);
                        }
                    }
                    else
                    {
                        if (movement.isOnFloor)
                        {
                            // Check if slope down

                            // Check if we still stand on solid ground
                            // bool stillIsOnFloor = IsOnFloor(origin + horizontalVector, movement);

                            if (movement.storedImpulse.y <= 0)
                            {
                                // Could we use a slope?

                                // Cast down from next point
                                RaycastHit castDownFromNextPointSlopeHit;
                                bool castDownFromNextPointSlope = Physics.Raycast(origin + horizontalVector, Vector3.down, out castDownFromNextPointSlopeHit, movement.slopeHeight + CritterMovementComponent.verticalBumper * 2);
                                Debug.DrawRay(origin + horizontalVector, Vector3.down * (movement.slopeHeight + CritterMovementComponent.verticalBumper * 2), Color.blue, 1, true);

                                if (castDownFromNextPointSlope)
                                {
                                    // place character here
                                    translate += new Vector3(0, -castDownFromNextPointSlopeHit.distance + CritterMovementComponent.verticalBumper, 0);
                                }
                                else
                                {
                                    movement.shouldJump = true;
                                }
                            }
                        }
                        // Apply motion
                        translate += horizontalVector;
                    }
                }
                transform.position += translate;
            }

            movement.shouldJump = movement.shouldJump && movement.isOnFloor;

            // Gravity cast
            {
                Vector3 translate = Vector3.zero;
                Vector3 origin = transform.position + movement.legs;

                RaycastHit hitInfo;

                bool isHit = Physics.Raycast(
                    origin,
                    new Vector3(0, Mathf.Sign(movement.storedImpulse.y), 0),
                    out hitInfo,
                    Mathf.Abs(movement.storedImpulse.y) + CritterMovementComponent.verticalBumper
                );

                // Debug.DrawRay(transform.position + legs, Vector3.down * Mathf.Abs(storedImpulse.y), Color.red, 0, false);

                float distance = movement.storedImpulse.y;
                if (isHit)
                {
                    distance = (hitInfo.distance - CritterMovementComponent.verticalBumper) * Mathf.Sign(movement.storedImpulse.y);
                    movement.isOnFloor = movement.storedImpulse.y < 0;
                }
                else
                {
                    movement.isOnFloor = false;
                }
                translate += new Vector3(0, distance, 0);

                transform.position += translate;
            }

            if (movement.isOnFloor)
            {
                controllableCritter.ResetVerticalImpulse();
            }

            // emit event shouldJump
            if (movement.shouldJump)
            {
                controllableCritter.Jump();
                movement.isOnFloor = false;
            }

            movement.shouldJump = false;
            movement.storedImpulse = new Vector3();
        }
    }

    // bool IsOnFloor(Vector3 position, CritterMovementComponent movement)
    // {
    //     if (movement.storedImpulse.y <= 0)
    //     {
    //         RaycastHit hit;
    //         bool isHit = Physics.Raycast(
    //             position,
    //             Vector3.down,
    //             out hit,
    //             CritterMovementComponent.verticalBumper * 2
    //         );
    //         return isHit && hit.distance < CritterMovementComponent.verticalBumper;
    //     }
    //     return false;
    // }
}