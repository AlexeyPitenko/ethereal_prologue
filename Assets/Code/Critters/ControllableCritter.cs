﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

public class ControllableCritter : MonoBehaviour
{

    public Vector3 impulse;
    public Vector3 desire;
    public float horizontalDrag = 0.8f;
    public float verticalDrag = 1f;
    public float acceleration = .5f;
    public float gravity = -0.3f;


    // TODO: Move to some animation class

    public enum FaceDirection { Up, Down, Left, Right }
    public FaceDirection faceDirection = FaceDirection.Down;

    // CritterMovementComponent critterMovementController;
    // Use this for initialization
    void Start()
    {
        // critterMovementController = GetComponent<CritterMovementComponent>();
    }

    public void Jump()
    {
        AddRawImpulse(new Vector3(0, .043f, 0));
    }

    public void ResetVerticalImpulse()
    {
        SetRawImpulse(new Vector3(impulse.x, 0, impulse.z));
    }


    public void SetDesire(Vector3 desire)
    {
        this.desire = desire;
    }
    public void AddRawImpulse(Vector3 rawImpulse)
    {
        impulse += rawImpulse;
    }

    public void SetRawImpulse(Vector3 rawImpulse)
    {
        impulse = rawImpulse;
    }

    public Vector3 GetRawImpulse()
    {
        return impulse;
    }

}

[UpdateAfter(typeof(ControlSystem))]
public class ControllableCritterSystem : ComponentSystem
{
    public struct State
    {
        public Transform transform;
        public ControllableCritter controllableCritter;
        public CritterMovementComponent movement;
    }

    protected override void OnUpdate()
    {
        var entities = GetEntities<State>();
        foreach (var entity in entities)
        {
            // Transform transform = entity.transform;
            ControllableCritter controllableCritter = entity.controllableCritter;
            CritterMovementComponent movement = entity.movement;

            if (controllableCritter.desire.magnitude > 1)
            {
                controllableCritter.desire.Normalize();
            }
            controllableCritter.impulse = new Vector3(controllableCritter.impulse.x, 0, controllableCritter.impulse.z) * controllableCritter.horizontalDrag + new Vector3(0, controllableCritter.impulse.y, 0) * controllableCritter.verticalDrag;

            controllableCritter.impulse += controllableCritter.desire * controllableCritter.acceleration * Time.deltaTime;
            controllableCritter.impulse += new Vector3(0, controllableCritter.gravity * Time.deltaTime, 0);

            movement.Move(controllableCritter.impulse);


            Animation2D animation2d = entity.transform.GetComponentInChildren<Animation2D>();
            CritterAnimations animationPackLea = entity.transform.GetComponentInChildren<CritterAnimations>();

            if (controllableCritter.desire.magnitude > 0.005f)
            {
                Vector3 direction = controllableCritter.desire.normalized;
                if (direction.z > 0.707106781f)
                {
                    controllableCritter.faceDirection = ControllableCritter.FaceDirection.Up;
                }
                else if (direction.z < -0.707106781f)
                {
                    controllableCritter.faceDirection = ControllableCritter.FaceDirection.Down;
                }
                else if (direction.x > 0.707106781f)
                {
                    controllableCritter.faceDirection = ControllableCritter.FaceDirection.Right;
                }
                else
                {
                    controllableCritter.faceDirection = ControllableCritter.FaceDirection.Left;
                }
            }

            if (movement.isOnFloor)
            {
                if (controllableCritter.impulse.magnitude > 0.005f)
                {
                    switch (controllableCritter.faceDirection)
                    {
                        case ControllableCritter.FaceDirection.Up:
                            animation2d.Set(animationPackLea.run_up);
                            break;
                        case ControllableCritter.FaceDirection.Down:
                            animation2d.Set(animationPackLea.run_down);
                            break;
                        case ControllableCritter.FaceDirection.Left:
                            animation2d.Set(animationPackLea.run_left);
                            break;
                        case ControllableCritter.FaceDirection.Right:
                            animation2d.Set(animationPackLea.run_right);
                            break;
                    }
                }
                else
                {
                    switch (controllableCritter.faceDirection)
                    {
                        case ControllableCritter.FaceDirection.Up:
                            animation2d.Set(animationPackLea.idle_stand_up);
                            break;
                        case ControllableCritter.FaceDirection.Down:
                            animation2d.Set(animationPackLea.idle_stand_down);
                            break;
                        case ControllableCritter.FaceDirection.Left:
                            animation2d.Set(animationPackLea.idle_stand_left);
                            break;
                        case ControllableCritter.FaceDirection.Right:
                            animation2d.Set(animationPackLea.idle_stand_right);
                            break;
                    }
                }
            }
            else
            {
                // flying!

                switch (controllableCritter.faceDirection)
                {
                    case ControllableCritter.FaceDirection.Up:
                        animation2d.Set(animationPackLea.flying_up);
                        break;
                    case ControllableCritter.FaceDirection.Down:
                        animation2d.Set(animationPackLea.flying_down);
                        break;
                    case ControllableCritter.FaceDirection.Left:
                        animation2d.Set(animationPackLea.flying_left);
                        break;
                    case ControllableCritter.FaceDirection.Right:
                        animation2d.Set(animationPackLea.flying_right);
                        break;
                }

            }


            controllableCritter.desire = Vector3.zero;
        }
    }
}