﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraControl : MonoBehaviour
{

    enum CameraMode
    {
        Cutscene,
        MoveStick,
        HardStick
    }

    CameraMode mode = CameraMode.HardStick;

    public Vector3 cameraOffset = new Vector3(0, 3, -4);

    public ControllableCritter mainCharacter;

    // Update is called once per frame
    void LateUpdate()
    {
        if (mainCharacter)
        {
            if (mode == CameraMode.HardStick)
            {
                transform.position = mainCharacter.transform.position + cameraOffset;
            }
            transform.LookAt(mainCharacter.transform);
        }
    }
}
