﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class Animation2DClip
{
    public Sprite[] sprites;
    public float duration = 0.2f;
    public bool flipX = false;
}

public class Animation2D : MonoBehaviour
{
    public Animation2DClip clip;

    float currentCounter = 0f;
    int currentSprite = 0;

    SpriteRenderer spriteRenderer;

    // Use this for initialization
    void Start()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();

        if (clip.sprites.Length > 0)
        {
            spriteRenderer.sprite = clip.sprites[0];
        }
        currentCounter = clip.duration;
    }

    // Update is called once per frame
    void Update()
    {
        if (clip.sprites == null || clip.sprites.Length == 0)
        {
            return;
        }

        currentCounter -= Time.deltaTime;
        if (currentCounter <= 0)
        {
            currentCounter = clip.duration;
            currentSprite++;
            if (currentSprite >= clip.sprites.Length)
            {
                currentSprite = 0;
            }
            spriteRenderer.sprite = clip.sprites[currentSprite];
        }
    }

    public void Set(Animation2DClip clip)
    {
        if (clip != this.clip)
        {
            spriteRenderer.flipX = clip.flipX;
            if (clip.sprites != null && clip.sprites.Length > 0)
            {
                spriteRenderer.sprite = clip.sprites[0];
                currentCounter = clip.duration;
                currentSprite = 0;
            }
            this.clip = clip;
        }
    }
}
