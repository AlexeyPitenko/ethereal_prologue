using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CritterAnimations : MonoBehaviour
{
    public Animation2DClip move_up = new Animation2DClip();
    public Animation2DClip move_down = new Animation2DClip();
    public Animation2DClip move_left = new Animation2DClip();
    public Animation2DClip move_right = new Animation2DClip();

    public Animation2DClip run_up = new Animation2DClip();
    public Animation2DClip run_down = new Animation2DClip();
    public Animation2DClip run_left = new Animation2DClip();
    public Animation2DClip run_right = new Animation2DClip();

    public Animation2DClip idle_stand_up = new Animation2DClip();
    public Animation2DClip idle_stand_down = new Animation2DClip();
    public Animation2DClip idle_stand_left = new Animation2DClip();
    public Animation2DClip idle_stand_right = new Animation2DClip();

    public Animation2DClip flying_up = new Animation2DClip();
    public Animation2DClip flying_down = new Animation2DClip();
    public Animation2DClip flying_left = new Animation2DClip();
    public Animation2DClip flying_right = new Animation2DClip();
}

public static class LeaAnimations_Loader
{
    public static Animation2DClip move_up = new Animation2DClip();
    public static Animation2DClip move_down = new Animation2DClip();
    public static Animation2DClip move_left = new Animation2DClip();
    public static Animation2DClip move_right = new Animation2DClip();

    public static Animation2DClip run_up = new Animation2DClip();
    public static Animation2DClip run_down = new Animation2DClip();
    public static Animation2DClip run_left = new Animation2DClip();
    public static Animation2DClip run_right = new Animation2DClip();

    static LeaAnimations_Loader()
    {
        Sprite[] sprites = Resources.LoadAll<Sprite>("");

        // move_up.sprites = new Sprite[4];
        // move_up.sprites[0] = sprites[0];
        // move_up.sprites[1] = sprites[1];
        // move_up.sprites[2] = sprites[2];
        // move_up.sprites[3] = sprites[1];
        // move_up.duration = 0.25f;

        // run_up.sprites = new Sprite[6];
        // run_up.sprites[0] = sprites[3];
        // run_up.sprites[1] = sprites[4];
        // run_up.sprites[2] = sprites[5];
        // run_up.sprites[3] = sprites[6];
        // run_up.sprites[4] = sprites[7];
        // run_up.sprites[5] = sprites[8];
        // run_up.duration = 0.16f;

        // run_right.sprites = new Sprite[6];
        // run_right.sprites[0] = sprites[35];
        // run_right.sprites[1] = sprites[36];
        // run_right.sprites[2] = sprites[37];
        // run_right.sprites[3] = sprites[38];
        // run_right.sprites[4] = sprites[39];
        // run_right.sprites[5] = sprites[40];
        // run_right.duration = 0.16f;

        // run_down.sprites = new Sprite[6];
        // run_down.sprites[0] = sprites[64];
        // run_down.sprites[1] = sprites[65];
        // run_down.sprites[2] = sprites[66];
        // run_down.sprites[3] = sprites[67];
        // run_down.sprites[4] = sprites[68];
        // run_down.sprites[5] = sprites[69];
        // run_down.duration = 0.16f;

        // run_left.sprites = new Sprite[6];
        // run_left.sprites[0] = sprites[35];
        // run_left.sprites[1] = sprites[36];
        // run_left.sprites[2] = sprites[37];
        // run_left.sprites[3] = sprites[38];
        // run_left.sprites[4] = sprites[39];
        // run_left.sprites[5] = sprites[40];
        // run_left.duration = 0.16f;
        // run_left.flipX = true;
    }

}