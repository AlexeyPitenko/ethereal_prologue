﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BillboardAngles : MonoBehaviour
{

    // Use this for initialization
    void Start()
    {
        StartCoroutine(SetAngles());
    }

    IEnumerator SetAngles()
    {
        yield return new WaitForSeconds(0.05f);
        transform.rotation = Camera.main.transform.rotation;
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Camera.main.transform.rotation;
    }
}
