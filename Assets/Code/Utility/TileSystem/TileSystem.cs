﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;

[System.Serializable]
public struct int2
{
    public int x;
    public int y;
}

// public struct byte2
// {
// 	byte x;
// 	byte y;
// }

// public struct TileImage 
// {
// 	byte tilesetID = 0;
// 	byte2 index;
// }

// Represents solid matter.
[System.Serializable]
public struct TileRange
{
    public byte start;
    public byte end;

    // Image info WIP
    // TileImage 
}

[System.Serializable]
public struct Tile
{
    public int2 geographicalIndex; // == world position / level.tileWidth

    // Must be sorted; must not overlap.
    public TileRange[] ranges;
}

[System.Serializable]
public class Tileset
{
    public Sprite image;
}

// public string Serialize()
// {
//     return JsonUtility.ToJson(this);
// }

public class TileSystem : ComponentSystem
{
    // This class can generate two things:

    // - The collision boxes -- in full 3D

    // - The image - in 2d


    const float tileHeightMultiplier = 0.25f;

    public struct State
    {
        public TiledLevel level;
        public MeshFilter meshFilter;
        public MeshCollider meshCollider;
    }

    bool started = false;

    protected override void OnUpdate()
    {
        if (!started)
        {
            GenerateColliders();
            started = true;
        }
    }

    protected void GenerateColliders()
    {
        var entities = GetEntities<State>();

        foreach (var entity in entities)
        {
            TiledLevel level = entity.level;

            List<Vector3> vertices = new List<Vector3>();
            List<int> indices = new List<int>();

            List<Vector2> uv = new List<Vector2>();

            foreach (Tile tile in level.tiles)
            {
                // Tileset tileset = tile.tilesetId[level.tilesets];
                float x_start = tile.geographicalIndex.x * level.tileSize.x + level.worldCoordinates.x;
                float z_start = tile.geographicalIndex.y * level.tileSize.y + level.worldCoordinates.y;

                float x_end = x_start + level.tileSize.x;
                float z_end = z_start + level.tileSize.y;

                // TileRange[] ranges = SortTileRanges(tile.ranges); // TODO move out somewhere

                // Sort tile ranges
                foreach (TileRange range in tile.ranges)
                {
                    int maxIndex = vertices.Count;

                    float y_start = ((float)range.start) * tileHeightMultiplier;
                    float y_end = ((float)range.end) * tileHeightMultiplier;

                    // Generate verticess
                    vertices.Add(new Vector3(x_start, y_start, z_start));
                    vertices.Add(new Vector3(x_end, y_start, z_start));
                    vertices.Add(new Vector3(x_end, y_start, z_end));
                    vertices.Add(new Vector3(x_start, y_start, z_end));
                    vertices.Add(new Vector3(x_start, y_end, z_start));
                    vertices.Add(new Vector3(x_end, y_end, z_start));
                    vertices.Add(new Vector3(x_end, y_end, z_end));
                    vertices.Add(new Vector3(x_start, y_end, z_end));

                    uv.Add(new Vector2(0, 0));
                    uv.Add(new Vector2(1, 0));
                    uv.Add(new Vector2(1, 1));
                    uv.Add(new Vector2(0, 1));

                    uv.Add(new Vector2(0, 0));
                    uv.Add(new Vector2(1, 0));
                    uv.Add(new Vector2(1, 1));
                    uv.Add(new Vector2(0, 1));

                    // generate triangles
                    indices.Add(0 + maxIndex);
                    indices.Add(1 + maxIndex);
                    indices.Add(2 + maxIndex);

                    indices.Add(0 + maxIndex);
                    indices.Add(2 + maxIndex);
                    indices.Add(3 + maxIndex);


                    indices.Add(6 + maxIndex);
                    indices.Add(5 + maxIndex);
                    indices.Add(4 + maxIndex);

                    indices.Add(4 + maxIndex);
                    indices.Add(7 + maxIndex);
                    indices.Add(6 + maxIndex);


                    indices.Add(5 + maxIndex);
                    indices.Add(2 + maxIndex);
                    indices.Add(1 + maxIndex);

                    indices.Add(5 + maxIndex);
                    indices.Add(6 + maxIndex);
                    indices.Add(2 + maxIndex);


                    indices.Add(0 + maxIndex);
                    indices.Add(3 + maxIndex);
                    indices.Add(4 + maxIndex);

                    indices.Add(3 + maxIndex);
                    indices.Add(7 + maxIndex);
                    indices.Add(4 + maxIndex);


                    indices.Add(3 + maxIndex);
                    indices.Add(2 + maxIndex);
                    indices.Add(7 + maxIndex);

                    indices.Add(2 + maxIndex);
                    indices.Add(6 + maxIndex);
                    indices.Add(7 + maxIndex);


                    indices.Add(1 + maxIndex);
                    indices.Add(0 + maxIndex);
                    indices.Add(4 + maxIndex);

                    indices.Add(1 + maxIndex);
                    indices.Add(4 + maxIndex);
                    indices.Add(5 + maxIndex);

                }
            }

            entity.meshFilter.mesh.Clear();
            entity.meshFilter.mesh.vertices = vertices.ToArray();
            entity.meshFilter.mesh.triangles = indices.ToArray();
            entity.meshFilter.mesh.uv = uv.ToArray();



            entity.meshFilter.mesh.RecalculateNormals();
            entity.meshFilter.mesh.RecalculateTangents();
            entity.meshFilter.mesh.RecalculateBounds();

            entity.meshCollider.sharedMesh = entity.meshFilter.mesh;
        }
    }




    // TileRange[] SortTileRanges(TileRange[] ranges)
    // {
    // 	ranges.Sort((a, b) => a.start > b.start);

    // 	for(int i = 0 ; i < ranges.Length - 1; i++)
    // 	{
    // 		if(ranges[i].start > ranges[i+1].start)
    // 		{
    // 			Debug.LogError("Range overlap");
    // 		}
    // 	}

    // 	return ranges;
    // }

    // void GenerateImage()
    // {

    // }
}


// [UpdateAfter(typeof(PlayerInputComponent))]
