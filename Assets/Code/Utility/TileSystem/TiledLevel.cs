﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class TiledLevel : MonoBehaviour
{
    public int2 worldCoordinates; // an offset
    public int2 tileSize; // grid size
	
	public Tileset[] tilesets;
	public Tile[] tiles;
}
